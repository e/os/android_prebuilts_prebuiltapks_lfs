LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_ARCH), $(filter $(TARGET_ARCH), arm64))
    APK_PATH := arm64
else ifeq ($(TARGET_ARCH), $(filter $(TARGET_ARCH), arm))
    APK_PATH := arm
else ifeq ($(TARGET_ARCH), $(filter $(TARGET_ARCH), x86_64))
    APK_PATH := x64
else ifeq ($(TARGET_ARCH), $(filter $(TARGET_ARCH), x86))
    APK_PATH := x86
endif

include $(CLEAR_VARS)
LOCAL_MODULE := Browser
LOCAL_MODULE_CLASS := APPS
LOCAL_PRODUCT_MODULE := true
LOCAL_MULTILIB := both
ifneq (,$(wildcard user-keys/platform.x509.pem))
LOCAL_CERTIFICATE := user-keys/platform
else
LOCAL_CERTIFICATE := platform
endif
LOCAL_REQUIRED_MODULES := \
        TrichromeLibrary
LOCAL_SRC_FILES := $(APK_PATH)/TrichromeChrome.apk
ifneq ($(call math_gt_or_eq, $(PLATFORM_SDK_VERSION), 31),)
LOCAL_OPTIONAL_USES_LIBRARIES := androidx.window.extensions androidx.window.sidecar
endif
LOCAL_OVERRIDES_PACKAGES := Jelly Browser2 QuickSearchBox
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := TrichromeLibrary
LOCAL_MODULE_CLASS := APPS
LOCAL_PRODUCT_MODULE := true
LOCAL_MULTILIB := both
ifneq (,$(wildcard user-keys/platform.x509.pem))
LOCAL_CERTIFICATE := user-keys/platform
else
LOCAL_CERTIFICATE := platform
endif
LOCAL_SRC_FILES := $(APK_PATH)/TrichromeLibrary.apk
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := BrowserWebView
LOCAL_MODULE_CLASS := APPS
LOCAL_PRODUCT_MODULE := true
LOCAL_MULTILIB := both
ifneq (,$(wildcard user-keys/platform.x509.pem))
LOCAL_CERTIFICATE := user-keys/platform
else
LOCAL_CERTIFICATE := platform
endif
LOCAL_REQUIRED_MODULES := \
        libwebviewchromium_loader \
        libwebviewchromium_plat_support
LOCAL_REQUIRED_MODULES += \
        TrichromeLibrary
LOCAL_SRC_FILES := $(APK_PATH)/TrichromeWebView.apk
ifneq ($(call math_gt_or_eq, $(PLATFORM_SDK_VERSION), 31),)
LOCAL_OPTIONAL_USES_LIBRARIES := androidx.window.extensions
endif
LOCAL_OVERRIDES_PACKAGES := webview
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)
