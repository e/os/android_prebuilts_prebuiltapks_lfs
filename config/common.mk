# Copyright (C) 2025 E FOUNDATION
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/%

PRODUCT_PACKAGES += \
    AccountManager \
    AdvancedPrivacy \
    AppLounge \
    BlissWeather \
    Browser \
    BrowserWebView \
    Camera \
    eCalendar \
    eDrive \
    FakeStore \
    GmsCore \
    GsfProxy \
    Mail \
    MurenaMDM \
    Notes \
    Ntfy \
    OpenKeychain \
    ParentalControl \
    PwaPlayer \
    Talkback \
    Tasks \
    WebCalendarManager

# Optional applications
MINIMAL_APPS ?= false

ifeq ($(MINIMAL_APPS),false)
PRODUCT_PACKAGES += \
    Maps \
    PdfViewer
endif

# Parental control
SYSTEM_EXT_PRIVATE_SEPOLICY_DIRS += prebuilts/prebuiltapks/ParentalControl/sepolicy

# BlissLauncher3
ifeq ($(PLATFORM_SDK_VERSION), 32)
PRODUCT_PACKAGES += BlissLauncher3_A12
else ifeq ($(PLATFORM_SDK_VERSION), 33)
PRODUCT_PACKAGES += BlissLauncher3_A13
else ifeq ($(PLATFORM_SDK_VERSION), 34)
PRODUCT_PACKAGES += BlissLauncher3_A14
endif
